package com.udemy.sfg.s03httpbasicauth.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
@WebMvcTest
public class StudentControllerTests {
    @Autowired
    WebApplicationContext wac;

    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    // username and password in optional, but annotation is mandatory
    // @WithMockUser(username = "anything", password = "anything")
    // @WithMockUser(username = "anything")
    @WithMockUser
    @Test
    void getStudents() throws Exception{
        mockMvc.perform(get("/students"))
                .andExpect(status().isOk());
    }

    @Test
    void testHttpBasic() throws Exception{
        mockMvc.perform(get("/students")
                // this will fail because we specified differ credential in main -> application.properties
                //.with(httpBasic("testuser", "testpassword")))
                // this will pass because we specified same credential in main -> application.properties
                .with(httpBasic("admin", "admin")))
                .andExpect(status().isOk());
    }
}
