package com.udemy.sfg.s03httpbasicauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S03HttpBasicAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(S03HttpBasicAuthApplication.class, args);
    }

}
