# Http Basic Auth

## References
```ref
1. https://docs.spring.io/spring-security/site/docs/current/reference/html5
```

## L18 - default spring boot configuration
```
1. add below dependency in pom
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-security</artifactId>
</dependency>

2. start the server
- default 
  -- username - user
  -- password - check console
  
3. 
- UI - we will see login page, pass above credential to access api from UI
- Postman
  -- way1 -- add header as 
    --- key as "Authorization"
    --- value as "Basic base64Ecoded(username:password)"
    --- eg. Authorization:Basic dXNlcjpmODRhNjMyNC1lOWM3LTQwYmQtOTNhOS04ZWRmNmE5Njc1MWU=
   -- way2 -- pass username and pass as part of url
     --- usename:password@ip:port/<uri>
     --- eg. admin:admin@127.0.0.1:8080/students
4. Spring boot auto configuration
- spring boot auto configure login page for UI, whenever we access any APIs
  -- once login the credential stored in cookie as JSESSION_ID, to clear it we need to logout
- spring boot auto configure logout for UI
  -- to access logout follow
  -- {baseEndpoint}/logout
  -- e.g. http://127.0.0.1:8080/logout 
```

## L19 - customizing default spring boot configuration
```
1.
In L18 we have seen than spring boot auto configure 
  username as "user",
  password is auto generated at the time of bootstrap.

2.
To customize(override) default behavior, we need to update application.properties file, as shown below
  spring.security.user.name=admin
  spring.security.user.password=${spring.security.user.name}


```

## L20 - JUnit API Testing 
```
refer - com.udemy.sfg.s03httpbasicauth.controller.StudentControllerTests -> getStudents
```

## L21 - JUnit Http Basic auth Testing
```
1.
refer - com.udemy.sfg.s03httpbasicauth.controller.StudentControllerTests -> testHttpBasic

2. @Test vs @Test +  @WithMockUser
- When we use @Test it will call the api pass through all servlet filters
- to access API we need to pass Authorization header using httpBasic("testuser", "testpassword"))
  -- refer - com.udemy.sfg.s03httpbasicauth.controller.StudentControllerTests -> getStudents
- when we use  @Test + @WithMockUser it skips servelet filters
  -- refer - com.udemy.sfg.s03httpbasicauth.controller.StudentControllerTests -> testHttpBasic

```

## L22 - SpringSecurityFilterChain
```
1. All the SpringSecurity things happens before spring servelet(web.xml -> filters -> spring-servlet) 
2. refer - kaushiwould-google-drive/draw.io/spring-security/udemy/spring-security-core-beginner-to-guru/s3/S3L22
3. refer ppt as ./L22-SpringSecurityFilterChain.pdf
```